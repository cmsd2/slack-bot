#!/bin/bash

export REST_API_GW_ID=`\
    aws --region ${AWS_REGION} apigateway get-rest-apis | \
    jq -r '.items[] | select(.name == "SlackBotApi") | .id' \
    `

aws apigateway create-deployment    \
    --rest-api-id ${REST_API_GW_ID} \
    --region ${AWS_REGION}          \
    --stage-name dev                \
    --variables LambdaAlias=DEV