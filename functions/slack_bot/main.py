"""
Lambda
"""

import slack_bot


def handle(event, context):
    """
    Lambda handler
    """

    return slack_bot.handle(event, context)
