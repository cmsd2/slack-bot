"""
Slack Bot Lambda
"""

import logging
import os
import os.path
import time
import weather

# logging.basicConfig(
#     format='%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s',
#     datefmt='%m-%d %H:%M:%S')
LOGGER = logging.getLogger('slack_bot')
LOGGER.setLevel(logging.INFO)

WEATHER = weather.Weather()


def handle(event, context):
    """
    Handle the lambda.
    """
    LOGGER.info("Received event: %s", event)

    if 'data' not in event:
        raise Exception('Bad Request: No Data')

    data = event['data']

    if 'token' not in data or data['token'] != os.environ['SLACK_TOKEN']:
        LOGGER.info("Invalid Token")
        raise Exception('Access Denied: Invalid Token')

    if 'command' not in data:
        raise Exception('Bad Request: No Command')

    if data['command'] == '/hello':
        return handle_hello(data, context)

    if data['command'] == '/weather':
        return handle_weather(data, context)

    else:
        LOGGER.info("Unrecognised command: %s", data['command'])
        raise Exception('Bad Request: Unrecognised Command')


def handle_hello(_data, _context):
    """
    Say hello.
    """
    LOGGER.info("Handling hello command")
    return {
        'text': 'Hello!',
        'response_type': 'in_channel'
    }


def handle_weather(data, _context):
    """
    Print the weather
    """
    LOGGER.info("Handling weather command")
    (current, forecast) = WEATHER.get_weather(data['text'])
    return {
        'text': current,
        'attachments': [
            {
                'text': forecast
            }
        ],
        'response_type': 'in_channel'
    }


def log_version():
    """
    Log the version
    """
    with open('version.txt', 'r') as version:
        LOGGER.info(version.read())


def log_time(msg, func, *args):
    """
    Log the time it takes to run a function
    """
    start = time.clock()
    result = func(*args)
    LOGGER.info("%s in %d seconds", msg, time.clock() - start)
    return result


def current_time_millis():
    """
    Get the current time millis
    """
    return int(round(time.time() * 1000))
