# -*- coding: utf-8 -*-
import json
import logging
import os
import unittest
from unittest.mock import MagicMock

import slack_bot

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(name)-12s %(levelname)-8s %(message)s',
    datefmt='%m-%d %H:%M:%S')

os.environ['SLACK_TOKEN'] = 'foo'

class SlackBotUnitTests(unittest.TestCase):
    def test_weather(self):
        weather_json = {
            "query": {
            "count": 1,
            "created": "2017-06-16T21:38:24Z",
            "lang": "en-US",
            "results": {
            "channel": {
                "units": {
                "distance": "mi",
                "pressure": "in",
                "speed": "mph",
                "temperature": "F"
                },
                "title": "Yahoo! Weather - London, England, GB",
                "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-44418/",
                "description": "Yahoo! Weather for London, England, GB",
                "language": "en-us",
                "ttl": "60",
                "location": {
                "city": "London",
                "country": "United Kingdom",
                "region": " England"
                },
                "wind": {
                "chill": "72",
                "direction": "270",
                "speed": "11"
                },
                "atmosphere": {
                "humidity": "56",
                "pressure": "1022.0",
                "rising": "0",
                "visibility": "16.1"
                },
                "astronomy": {
                "sunrise": "4:43 am",
                "sunset": "9:20 pm"
                },
                "image": {
                "title": "Yahoo! Weather",
                "width": "142",
                "height": "18",
                "link": "http://weather.yahoo.com",
                "url": "http://l.yimg.com/a/i/brand/purplelogo//uh/us/news-wea.gif"
                },
                "item": {
                "title": "Conditions for London, England, GB at 09:00 PM BST",
                "lat": "51.506401",
                "long": "-0.12721",
                "link": "http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-44418/",
                "pubDate": "Fri, 16 Jun 2017 09:00 PM BST",
                "condition": {
                "code": "26",
                "date": "Fri, 16 Jun 2017 09:00 PM BST",
                "temp": "72",
                "text": "Cloudy"
                },
                "forecast": [
                {
                "code": "28",
                "date": "16 Jun 2017",
                "day": "Fri",
                "high": "76",
                "low": "55",
                "text": "Mostly Cloudy"
                },
                {
                "code": "39",
                "date": "25 Jun 2017",
                "day": "Sun",
                "high": "74",
                "low": "61",
                "text": "Scattered Showers"
                }
                ],
                "description": "<![CDATA[<img src=\"http://l.yimg.com/a/i/us/we/52/26.gif\"/>\n<BR />\n<b>Current Conditions:</b>\n<BR />Cloudy\n<BR />\n<BR />\n<b>Forecast:</b>\n<BR /> Fri - Mostly Cloudy. High: 76Low: 55\n<BR /> Sat - Partly Cloudy. High: 80Low: 62\n<BR /> Sun - Sunny. High: 85Low: 63\n<BR /> Mon - Partly Cloudy. High: 86Low: 65\n<BR /> Tue - Mostly Cloudy. High: 78Low: 62\n<BR />\n<BR />\n<a href=\"http://us.rd.yahoo.com/dailynews/rss/weather/Country__Country/*https://weather.yahoo.com/country/state/city-44418/\">Full Forecast at Yahoo! Weather</a>\n<BR />\n<BR />\n(provided by <a href=\"http://www.weather.com\" >The Weather Channel</a>)\n<BR />\n]]>",
                "guid": {
                "isPermaLink": "false"
                }
                }
            }
            }
            }
            }
        slack_bot.WEATHER.query_weather = MagicMock(return_value=weather_json)
        request = {
            'data': {
                'token': 'foo',
                'command': '/weather',
                'text': 'london'
            }
        }
        expected_response = {
            'text': "Right now in London it's 22 degrees and cloudy",
            'attachments': [
                {
                    'text': "Forecast is mostly cloudy with a high of 24 and a low of 12"
                }
            ],
            'response_type': 'in_channel'
        }

        response = slack_bot.handle(request, {})
        self.assertEqual(response, expected_response)

    def test_hello_invalid_token(self):
        request = {
            'data': {
                'token': 'bar',
            }
        }

        self.assertRaises(Exception, slack_bot.handle, request, {})

    def test_hello_ok(self):
        request = {
            'data': {
                'token': 'foo',
                'team_id': 'team_123',
                'team_domain': 'myslackdomain',
                'channel_id': 'channel_123',
                'channel_name': 'myslackchannel',
                'user_id': 'user_123',
                'user_name': 'myusername',
                'command': '/hello',
                'text': '',
                'response_url': 'https://hooks.slack.example.com/commands/team123/45678/9abcdef'
            }
        }

        expected_response = {
            'text': 'Hello!',
            'response_type': 'in_channel'
        }

        response = slack_bot.handle(request, {})
        self.assertEqual(response, expected_response)


if __name__ == '__main__':
    unittest.main()
