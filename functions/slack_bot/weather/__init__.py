"""
Yahoo Weather module
"""

import json
import myql


class Weather:
    """
    Weather query object
    """

    def __init__(self):
        self.yql = myql.MYQL()

    def query_weather(self, place):
        """
        Get the weather for a place name
        """
        response = self.yql.raw_query(
            'select * from weather.forecast \
            where woeid in (\
            select woeid from geo.places(1) where text="{}"\
            )'.format(place),
            format='json')
        response_json = json.loads(response.content)
        return response_json

    def get_weather(self, place):
        """
        Get the current temperature and forecast
        """
        if place is None or place == '':
            place = 'london, uk'
        current = 'weather'
        forecast = 'forecast'
        response_json = self.query_weather(place)
        weather = response_json['query']['results']['channel']
        units = weather['units']
        location = weather['location']
        target_units = {
            'temperature': 'C'
        }
        current = self.condition_to_str(
            location,
            weather['item']['condition'],
            units,
            target_units)
        forecast = self.forecast_to_str(
            weather['item']['forecast'][0],
            units,
            target_units)
        return (current, forecast)

    def condition_to_str(self, location, condition, units, target_units):
        """
        Convert the location and temperature to a description.
        """
        infix = 'with'
        if condition['text'].endswith('y'):
            infix = 'and'
        return "Right now in {} it\'s {} degrees {} {}".format(
            location['city'],
            self.convert_temp(condition['temp'], units, target_units),
            infix,
            condition['text'].lower())

    def forecast_to_str(self, forecast, units, target_units):
        """
        Convert the forecast to a description.
        """
        return "Forecast is {} with a high of {} and a low of {}".format(
            forecast['text'].lower(),
            self.convert_temp(forecast['high'], units, target_units),
            self.convert_temp(forecast['low'], units, target_units))

    def convert_temp(self, temp, units, target_units):
        """
        Convert temperature between celcius and fahrenheit.
        """
        temp = int(temp)
        if units['temperature'] == target_units['temperature']:
            return temp
        if units['temperature'] == 'F':
            return int(((temp - 32) * 5) / 9)
        else:
            return int(((temp * 9) / 5) - 32)
