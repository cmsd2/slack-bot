resource "aws_api_gateway_account" "demo" {
  cloudwatch_role_arn = "${aws_iam_role.api_gw_cloudwatch_role.arn}"
}

resource "aws_api_gateway_rest_api" "api_gw" {
    name = "SlackBotApi"
    description = "API for general use and for Slack Callbacks"
}

resource "aws_api_gateway_deployment" "api_gw_deployment" {
  depends_on = [
    "aws_api_gateway_integration.api_slack_hello_endpoint_post_to_lambda",
    "aws_api_gateway_method.api_slack_hello_endpoint_post"
  ]

  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  stage_name  = "dev"
#   stage_description = "slack_bot-${var.deployment_id}"

  variables = {
  }
}

resource "aws_api_gateway_stage" "dev" {
  stage_name = "dev"
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  deployment_id = "${aws_api_gateway_deployment.api_gw_deployment.id}"

  variables {
    LambdaAlias = "DEV"
    # DeploymentId = "${var.deployment_id}"
  }
}

resource "aws_api_gateway_base_path_mapping" "dev" {
  api_id      = "${aws_api_gateway_rest_api.api_gw.id}"
  stage_name  = "${aws_api_gateway_deployment.api_gw_deployment.stage_name}"
  domain_name = "${aws_api_gateway_domain_name.api_gw_dns.domain_name}"
}

resource "aws_api_gateway_resource" "api_slack_endpoint" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  parent_id = "${aws_api_gateway_rest_api.api_gw.root_resource_id}"
  path_part = "slack"
}

resource "aws_api_gateway_resource" "api_slack_hello_endpoint" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  parent_id = "${aws_api_gateway_resource.api_slack_endpoint.id}"
  path_part = "hello"
}

resource "aws_api_gateway_method" "api_slack_hello_endpoint_post" {
  rest_api_id   = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id   = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method   = "POST"
  authorization = "NONE"

  request_parameters = { "method.request.header.Content-Type" = true } 
}

resource "aws_api_gateway_integration" "api_slack_hello_endpoint_post_to_lambda" {
  rest_api_id             = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id             = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method             = "${aws_api_gateway_method.api_slack_hello_endpoint_post.http_method}"
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${var.apex_function_slack_bot}:$${stageVariables.LambdaAlias}/invocations"
  #uri                     = "${aws_lambda_function.lambda.invoke_arn}"
  credentials             = "${aws_iam_role.api_gw_lambda_role.arn}"
  passthrough_behavior    = "WHEN_NO_TEMPLATES"

  request_templates {
    "application/x-www-form-urlencoded" = <<EOF
{
    "data": {
        #foreach( $token in $input.path('$').split('&') )
            #set( $keyVal = $token.split('=') )
            #set( $keyValSize = $keyVal.size() )
            #if( $keyValSize >= 1 )
                #set( $key = $util.urlDecode($keyVal[0]) )
                #if( $keyValSize >= 2 )
                    #set( $val = $util.urlDecode($keyVal[1]) )
                #else
                    #set( $val = '' )
                #end
                "$key": "$val"#if($foreach.hasNext),#end
            #end
        #end
    }
}
EOF
  }
}

resource "aws_api_gateway_method_response" "api_slack_hello_endpoint_post_200" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method = "${aws_api_gateway_method.api_slack_hello_endpoint_post.http_method}"
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "api_slack_hello_endpoint_post_200" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method = "${aws_api_gateway_method.api_slack_hello_endpoint_post.http_method}"
  status_code = "${aws_api_gateway_method_response.api_slack_hello_endpoint_post_200.status_code}"
}

resource "aws_api_gateway_method_response" "api_slack_hello_endpoint_post_400" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method = "${aws_api_gateway_method.api_slack_hello_endpoint_post.http_method}"
  status_code = "400"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "api_slack_hello_endpoint_post_400" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method = "${aws_api_gateway_method.api_slack_hello_endpoint_post.http_method}"
  status_code = "${aws_api_gateway_method_response.api_slack_hello_endpoint_post_400.status_code}"

  selection_pattern = "^Bad Request:.*"
}

resource "aws_api_gateway_method_response" "api_slack_hello_endpoint_post_403" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method = "${aws_api_gateway_method.api_slack_hello_endpoint_post.http_method}"
  status_code = "403"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "api_slack_hello_endpoint_post_403" {
  rest_api_id = "${aws_api_gateway_rest_api.api_gw.id}"
  resource_id = "${aws_api_gateway_resource.api_slack_hello_endpoint.id}"
  http_method = "${aws_api_gateway_method.api_slack_hello_endpoint_post.http_method}"
  status_code = "${aws_api_gateway_method_response.api_slack_hello_endpoint_post_403.status_code}"

  selection_pattern = "^Access Denied:.*"
}