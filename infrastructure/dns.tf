resource "aws_api_gateway_domain_name" "api_gw_dns" {
  domain_name = "${var.domain_name}"

  certificate_arn = "${var.certificate_arn}"
}

resource "aws_route53_record" "api_gw_a_record" {
  zone_id = "${var.zone_id}"

  name = "${aws_api_gateway_domain_name.api_gw_dns.domain_name}"
  type = "A"

  alias {
    name                   = "${aws_api_gateway_domain_name.api_gw_dns.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.api_gw_dns.cloudfront_zone_id}"
    evaluate_target_health = true
  }
}
