resource "aws_lambda_alias" "slack_bot_latest" {
  name             = "DEV"
  description      = "slack_bot latest version"
  function_name    = "${var.apex_function_slack_bot}"
  function_version = "$LATEST"
}
