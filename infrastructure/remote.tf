terraform {
  backend "s3" {
    bucket = "cmsd2-config"
    key    = "slack-bot/infra/terraform.tfstate"
    region = "eu-west-1"
  }
}
