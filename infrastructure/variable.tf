variable "aws_region" {}

variable "account_id" {}

variable "apex_environment" {}

variable "apex_function_role" {}

variable "apex_function_slack_bot" {}

variable "apex_function_slack_bot_name" {}

variable "certificate_arn" {}

variable "domain_name" {}

variable "zone_id" {}

variable "deployment_id" {}
